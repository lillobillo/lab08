package it.unibo.oop.lab08.ex02;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * 
 */
public class Controller {

    /*
     * This class must implement a simple controller responsible of I/O access.
     * It considers a single file at a time, and it is able to serialize objects
     * in it.
     * 
     * Implement this class in such a way that:
     * 
     * 1) It has a method for setting a File as current file
     * 
     * 2) It has a method for getting the current File
     * 
     * 3) It has a method for getting the path (in form of String) of the
     * current File
     * 
     * 4) It has a method that gets a Serializable as input and saves such
     * Object in the current file. Remember how to use the ObjectOutputStream.
     * This method may throw IOException.
     * 
     * 5) By default, the current file is "output.dat" inside the user home
     * folder. A String representing the local user home folder can be accessed
     * using System.getProperty("user.home"). The separator symbol (/ on *nix, \
     * on Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods
     * leads to a software that run correctly on every platform.
     */

    private static final String DEFAULT_FILE = System.getProperty("user.home") + System.getProperty("file.separator")
            + "output.dat";

    private File currentFile = new File(DEFAULT_FILE);

    /**
     * @return the current file
     */
    public File getCurrentFile() {
        return currentFile;
    }

    /**
     * @param file
     *            the file to be set as the current file
     */
    public void setCurrentFile(final File file) {
        if (file.getParentFile().exists()) {
            this.currentFile = file;
        } else {
            throw new IllegalArgumentException("The specified file's parent folder does " + "not exist!");
        }
    }

    /**
     * @param file
     *            the file where to write
     */
    public void setCurrentFile(final String file) {
        setCurrentFile(new File(file));
    }

    /**
     * @return the current file's path
     */
    public String getCurrentFilePath() {
        return currentFile.getPath();
    }

    /**
     * @param obj
     *            the object to save in the file
     * @throws FileNotFoundException
     *             if the specified file does not exist
     * @throws IOException
     *             if writing fails
     */
    public void save(final Serializable obj) throws FileNotFoundException, IOException {
        try (final ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(currentFile))) {
            out.writeObject(obj);
        }
    }
}
