package it.unibo.oop.lab08.ex03;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import it.unibo.oop.lab08.ex02.Controller;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {

    /*
     * TODO: Starting from the application in ex02:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface. To see which should be the result, see ex03.png.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

    private final JFrame frame = new JFrame("My first Java graphical interface");

    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextArea with a button "Save"
     * right below (see "ex02.png" for the expected result). SUGGESTION: Use a
     * JPanel with BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The behavior of the program is that, if "Save" is pressed, the
     * controller is asked to save the file.
     * 
     * Use "ex02.png" (in the res directory) to verify the expected aspect.
     */

    /**
     * builds a new {@link SimpleGUI}.
     * 
     * @param ctrl
     *            a Controller for the application
     */
    public SimpleGUIWithFileChooser(final Controller ctrl) {

        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */

        final JPanel mainPanel = new JPanel(new BorderLayout());

        final JTextArea text = new JTextArea();
        mainPanel.add(text, BorderLayout.CENTER);

        final JButton save = new JButton("Save");
        save.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                try {
                    ctrl.save(text.getText());
                } catch (IOException e1) {
                    JOptionPane.showMessageDialog(frame, e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    e1.printStackTrace();
                }
            }
        });

        mainPanel.add(save, BorderLayout.SOUTH);

        final JTextField filePath = new JTextField(ctrl.getCurrentFilePath());
        filePath.setEditable(false);

        final JButton chooseFile = new JButton("Browse...");
        chooseFile.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(final ActionEvent e) {
                final JFileChooser fch = new JFileChooser(ctrl.getCurrentFilePath());
                final int saveRes = fch.showSaveDialog(frame);
                switch (saveRes) {
                case JFileChooser.APPROVE_OPTION:
                    final File newCurrentFile = fch.getSelectedFile();
                    ctrl.setCurrentFile(newCurrentFile);
                    filePath.setText(ctrl.getCurrentFilePath());
                    text.setText("Current file: " + newCurrentFile.getPath());
                    break;
                case JFileChooser.CANCEL_OPTION:
                    break;
                default:
                    JOptionPane.showMessageDialog(frame, saveRes, "Fucking error!!", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        final JPanel upperPanel = new JPanel(new BorderLayout());
        upperPanel.add(filePath, BorderLayout.CENTER);
        upperPanel.add(chooseFile, BorderLayout.LINE_END);
        mainPanel.add(upperPanel, BorderLayout.NORTH);

        frame.setContentPane(mainPanel);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);

        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
    }

    private void show() {
        frame.setVisible(true);
    }

    /**
     * @param args
     *            unused
     */
    public static void main(final String[] args) {
        new SimpleGUIWithFileChooser(new Controller()).show();
    }
}
